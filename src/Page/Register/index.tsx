import { Box } from '@material-ui/core'
import marceneira from 'assets/marcenaria.jpg'
import { PageContent } from 'components/PageContent'
import { RegisterForm } from 'components/RegisterForm'

export const Register = () => {
  return (
    <PageContent>
      <Box className="flex h-full  w-full border">
        <main className="z-30 flex w-[40%] items-center justify-center bg-[#fff4f0] bg-gradient-to-r	p-5	 shadow-black">
          <RegisterForm />
        </main>
        <Box className="block flex h-full w-[60%] justify-center bg-blue-700 align-middle">
          <img src={marceneira} />
        </Box>
      </Box>
    </PageContent>
  )
}
