import { Box } from '@material-ui/core'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import axios from 'axios'
import { useEffect, useState } from 'react'

import { BasePaper } from 'components/BasePaper'
import { Button } from 'components/Buttom'
import { GoBack } from 'components/GoBack'
import { PageContent } from 'components/PageContent'
import { TextFieldStyled } from 'components/TextFieldStyled'
import { TitlePage } from 'components/TitlePage'
import { baseURl } from 'services'
import { useNavigate, useParams } from 'react-router-dom'
import { Autocomplete } from '@material-ui/lab'

export const RegisterProduct = () => {
  const history = useNavigate()
  const [value, setValue] = useState('')
  const { id } = useParams()

  const formik = useFormik({
    initialValues: {
      name: '',
      material: '',

      size: '',
      width: '',
      totalValue: ''
    },
    validationSchema: Yup.object({
      name: Yup.string().required('Obrigatório'),
      material: Yup.string().required('Obrigatório'),
      size: Yup.string().required('Obrigatório'),
      width: Yup.string().required('Obrigatório'),
      totalValue: Yup.number().required('Obrigatório')
    }),
    onSubmit: (values) => {
      console.log(values)
      history('/catalogo')
    }
  })

  const handleLoginRequest = async (values: {
    name: string
    material: string
    size: string
    width: string
    totalValue: string
  }) => {
    await axios
      .post(baseURl + 'auth', { ...values })
      .then((response) => {
        sessionStorage.setItem('token', response.data.token)
        // setSucess(true)
        const timer = setTimeout(() => {
          history('/catalogo')
        }, 1000)
      })
      .catch((err) => {
        //setFailed(true)
      })
  }

  useEffect(() => {
    console.log(formik)
  }, [formik])

  useEffect(() => {
    onChangeField(value)
  }, [value])
  const onChangeField = (value: string) => {
    formik.values.material = value
  }
  return (
    <PageContent className="w-[1280px]">
      <Box className="border-3 h-full border-black bg-[url('/assets/marcenaria.jpg')]">
        <Box className=" flexbox border-1 w-[600px] flex-row justify-start border-red-500">
          <GoBack route="catalogo" />
          <TitlePage text={`${id ? 'Edição' : 'Cadastro'} do produto`} />
        </Box>

        <BasePaper className="border-1 mx-auto w-96  border-red-600">
          <form className="grid gap-5" onSubmit={formik.handleSubmit}>
            <TextFieldStyled
              name="name"
              placeholder="Nome do produto"
              label="Nome"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.name}
            />
            <Autocomplete
              id="combo-box-demo"
              options={['MDF']}
              value={value}
              getOptionLabel={(option) => option}
              onChange={(e: object, value: any) => {
                setValue(value)
              }}
              renderInput={(params) => (
                <TextFieldStyled
                  {...params}
                  label="Material do produto"
                  variant="outlined"
                  name="material"
                />
              )}
            />
            <TextFieldStyled
              name="size"
              placeholder="Tamanho"
              label="Tamanho"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.size}
            />{' '}
            <TextFieldStyled
              name="width"
              placeholder="Largura"
              label="Largura"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.width}
            />{' '}
            <TextFieldStyled
              name="totalValue"
              placeholder="Valor total"
              label="Valor total"
              type={'number'}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.totalValue}
            />
            <Button textButton="Enviar" type="submit" className="w-96" />
          </form>
        </BasePaper>
      </Box>
    </PageContent>
  )
}
