import { useEffect, useState } from 'react'
import { Box, FormControlLabel, Radio } from '@material-ui/core'
import TextField from '@material-ui/core/TextField'
import Autocomplete from '@material-ui/lab/Autocomplete'
import Checkbox from '@material-ui/core/Checkbox'
import * as Yup from 'yup'
import axios from 'axios'
import { Form, useFormik } from 'formik'

import { Button } from 'components/Buttom'
import { GoBack } from 'components/GoBack'
import { PageContent } from 'components/PageContent'
import { TextFieldStyled } from 'components/TextFieldStyled'
import { TitlePage } from 'components/TitlePage'
import { material } from 'utils/constants'
import { baseURl } from 'services'
import { useNavigate, useParams } from 'react-router-dom'

export const RegisterMaterial = () => {
  const history = useNavigate()
  const { id } = useParams()

  const formik = useFormik({
    initialValues: {
      name: '',
      value: 0
    },
    validationSchema: Yup.object({
      name: Yup.string().required('Obrigatório'),
      value: Yup.number().required('Obrigatório')
    }),
    onSubmit: (values) => {
      createOrEditMaterial(values)
    }
  })

  const [value, setValue] = useState('')
  const createOrEditMaterial = (values: { name: string; value: number }) => {
    // const response = await axios.post(baseURl + 'material/register')

    // console.log(response)
    if (id) {
      history('/catalogo')
    } else {
      history('/register-product')
    }
  }

  useEffect(() => {
    console.log
  }, [])
  const onChangeField = (value: string) => {
    formik.values.name = value
  }

  useEffect(() => {
    console.log(formik)
  }, [formik])

  useEffect(() => {
    onChangeField(value)
  }, [value])
  return (
    <PageContent className="w-[1280px]">
      <Box>
        <GoBack route="catalogo" />
        <TitlePage text={`${id ? 'Edição' : 'Cadastro'} de material`} />
      </Box>
      <Box className="border-1 mx-auto h-screen w-[600px] border-red-800">
        <form
          className="grid justify-center gap-5"
          onSubmit={formik.handleSubmit}
        >
          <Autocomplete
            id="combo-box-demo"
            options={material}
            value={value}
            getOptionLabel={(option) => option}
            onChange={(e: object, value: any) => {
              setValue(value)
            }}
            renderInput={(params) => (
              <TextField
                {...params}
                label="Material"
                variant="outlined"
                name="name"
              />
            )}
          />
          <TextFieldStyled
            name="value"
            placeholder="Valor"
            label="Valor"
            type={'number'}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.value}
          />
          <Button
            textButton="Cadastrar"
            name="submit"
            type="submit"
            className="w-96"
          />
        </form>
      </Box>
    </PageContent>
  )
}
