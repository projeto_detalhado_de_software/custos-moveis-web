import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { Box, Tooltip, Button as ButtonMui } from '@material-ui/core'

import { Button } from 'components/Buttom'
import { CardItemInfo } from 'components/CardItemInfo'
import { PageContent } from 'components/PageContent'
import { TitlePage } from 'components/TitlePage'
import { Tabs } from 'utils/constants'
import { ButtonTab } from 'components/ButtonTab'

export const Catalog = () => {
  const history = useNavigate()
  const [tab, setTabe] = useState(Tabs.products)

  return (
    <PageContent className="w-[1280px]">
      <Box className="mx-auto w-[1280px] p-2">
        <TitlePage text="Visualizar produtos" />
        <Box className=" ">
          <Box className="mx-auto mb-2 flex w-[1280px]  flex-row-reverse">
            <Box className="flex flex-col gap-1">
              <Tooltip title="Cadastrar seu novo produto">
                <Button
                  textButton="Cadastrar material"
                  onClick={() => history('/register-material')}
                  className="w-[150px]"
                />
              </Tooltip>{' '}
              <Tooltip title="Cadastrar seu novo Produto">
                <Button
                  textButton="Cadastrar Produto"
                  onClick={() => history('/register-product')}
                  className="w-[150px]"
                />
              </Tooltip>
            </Box>
          </Box>
          <Box>
            <ButtonTab
              onClick={() => {
                setTabe(Tabs.products)
              }}
            >
              Produtos Cadastrados
            </ButtonTab>
            <ButtonTab
              onClick={() => {
                setTabe(Tabs.materials)
              }}
            >
              Materiais Cadastrados
            </ButtonTab>
          </Box>
          <Box className="z-30 mx-auto  grid h-[500px] w-[1280px]	 grid-cols-4  gap-5 overflow-auto border-2 border-[#a77565] bg-white p-5">
            {tab === Tabs.products ? (
              <>
                <CardItemInfo
                  productName="Caneca de cerâmica rústica"
                  productprice="40,00"
                />{' '}
                <CardItemInfo
                  productName="Caneca de cerâmica rústica"
                  productprice="40,00"
                />{' '}
                <CardItemInfo
                  productName="Caneca de cerâmica rústica"
                  productprice="40,00"
                />{' '}
                <CardItemInfo
                  productName="Caneca de cerâmica rústica"
                  productprice="40,00"
                />{' '}
                <CardItemInfo
                  productName="Caneca de cerâmica rústica"
                  productprice="40,00"
                />{' '}
                <CardItemInfo
                  productName="Caneca de cerâmica rústica"
                  productprice="40,00"
                />{' '}
                <CardItemInfo
                  productName="Caneca de cerâmica rústica"
                  productprice="40,00"
                />{' '}
                <CardItemInfo
                  productName="Caneca de cerâmica rústica"
                  productprice="40,00"
                />{' '}
                <CardItemInfo
                  productName="Caneca de cerâmica rústica"
                  productprice="40,00"
                />
              </>
            ) : (
              <>
                <p>Teste</p>
              </>
            )}
          </Box>
        </Box>
      </Box>
    </PageContent>
  )
}
