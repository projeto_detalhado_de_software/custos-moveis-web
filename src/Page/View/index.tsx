import { Box, Typography } from '@material-ui/core'
import item from 'assets/itemTest.svg'
import { GoBack } from 'components/GoBack'

export const View = () => {
  return (
    <Box className="h-screen ">
      <Box className="p-5">
        <Box className=" m-auto w-[1280px] ">
          <Box className="mb-5">
            <GoBack />
          </Box>
          <main className="flex h-[580px]">
            <Box className="h-full w-[640px] bg-yellow-200">
              <img src={item} />
            </Box>
            <Box className="h-full w-[640px] p-5">
              <Typography variant="h4">Caneca de ceramica rustica</Typography>
              <Typography className="font-semibold	" variant="h6">
                RS 40,00
              </Typography>

              <Box className="mt-10">
                <Typography variant="body1">Descrição</Typography>
                <Box className="mt-3">
                  <Typography variant="body2">
                    Aqui vem um texto descritivo do produto, esta caixa de texto
                    servirá apenas de exemplo para que simule algum texto que
                    venha a ser inserido nesse campo, descrevendo tal produto.
                  </Typography>
                </Box>
              </Box>
            </Box>
          </main>
        </Box>
      </Box>
    </Box>
  )
}
