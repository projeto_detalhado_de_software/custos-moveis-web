export const material = [
  'MADEIRAPINUS',
  'MDF',
  'MDP',
  'LACA',
  'BP',
  'DURATREE',
  'ACOINOX',
  'PLASTICO',
  'MADEIRACOMVIDRO',
  'FERRO'
]
export enum Tabs {
  products,
  materials
}
