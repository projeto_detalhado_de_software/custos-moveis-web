import 'tailwindcss/tailwind.css'
import React from 'react'
import ReactDOM from 'react-dom/client'
import { createBrowserRouter, RouterProvider, Route } from 'react-router-dom'

import { Register } from 'Page/Register'
import { Home } from 'Page/Home'
import { Catalog } from 'Page/Catalog'
import { View } from 'Page/View'
import { RegisterProduct } from 'Page/RegisterProduct'
import { RegisterMaterial } from 'Page/RegisterMaterial'

const router = createBrowserRouter([
  {
    path: '/',
    element: <Home />
  },
  {
    path: '/Register',
    element: <Register />
  },
  {
    path: '/catalogo',
    element: <Catalog />
  },
  { path: 'view', element: <View /> },
  { path: 'register-product', element: <RegisterProduct /> },
  { path: 'edit-product/:id', element: <RegisterProduct /> },
  { path: 'register-material', element: <RegisterMaterial /> },
  { path: `edit-material/:id`, element: <RegisterMaterial /> }
])

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
)
