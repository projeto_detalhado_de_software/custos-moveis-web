import { TextField } from '@material-ui/core'
import { Formik, useFormik, FormikProps, Form as FormIK } from 'formik'
import * as Yup from 'yup'
export interface FieldsForms {
  [key: string]: string
  label: string
}

interface FormProps {
  initialValues: FieldsForms[]
  submit: (e: FieldsForms[]) => void
  textButton: string
}
interface FieldProps {
  name: string
  formik: FormikProps<any>
  error: boolean
  label: string
}

const Field = ({ name, formik, error, label }: FieldProps) => {
  return (
    <div className="mt1 flex  rounded-md border ">
      <TextField
        name={name}
        className="  w-full flex-1 rounded-none rounded-r-md border  border-blue-500 outline-none focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
        placeholder={name === 'email' ? 'exemple@gmail.com' : ''}
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        value={formik.values[name]}
        label={label}
        variant="outlined"
        size="small"
        error={error}
      />
    </div>
  )
}

export const Form = ({ initialValues, submit, textButton }: FormProps) => {
  const formik = useFormik({
    initialValues,
    onSubmit: (values) => {
      submit(values)
    }
  })

  const makeSchema = (object: FieldsForms[]) => {
    const keys = Object.keys(object)

    const shapeObject = keys.reduce(
      (old, current) => ({
        ...old,
        [current]: Yup.string().required('Required')
      }),
      {}
    )
    return Yup.object().shape(shapeObject)
  }
  return (
    <>
      <Formik
        initialValues={initialValues}
        onSubmit={submit}
        validationSchema={makeSchema(initialValues)}
      >
        {({ errors, touched }) => (
          <FormIK>
            <div className="grid w-80 gap-5">
              {initialValues.map((item, i) => (
                <>
                  <Field
                    name={item[0]}
                    key={i}
                    label={item.label}
                    formik={formik}
                    // error={
                    //   errors[`${item[0]}`] && touched[`${item[0]}`]
                    //     ? true
                    //     : false
                    // }
                  />
                </>
              ))}
              <button
                name="submit"
                type="submit"
                className=" h-10 rounded-md  border-none border-black bg-[#a77565] text-slate-50 hover:bg-[#311a07] focus:outline-none focus:ring-1"
              >
                {textButton}
              </button>
            </div>
          </FormIK>
        )}
      </Formik>
    </>
  )
}
