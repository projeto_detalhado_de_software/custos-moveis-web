import { Snackbar, TextField, Typography } from '@material-ui/core'
import logo from 'assets/custos-moveis.png'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import axios from 'axios'
import Alert from '@material-ui/lab/Alert'
import { useNavigate } from 'react-router-dom'

import { baseURl } from 'services'
import { formikValuesType } from 'models/types'
import { useState } from 'react'
import { Button } from 'components/Buttom'

export const RegisterForm = () => {
  const history = useNavigate()

  const validationSchema = Yup.object({
    username: Yup.string().required('Obrigatório'),
    password: Yup.string().required('Obrigatório'),
    email: Yup.string().email('email invalido').required('Obrigatório')
  })

  const formik = useFormik({
    initialValues: {
      username: '',
      password: '',
      email: ''
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      handleRegisterUser(values)
    }
  })

  const [sucesss, setSucess] = useState(false)

  const handleRegisterUser = async (values: formikValuesType) => {
    await axios
      .post(`${baseURl}register`, {
        ...values
      })
      .then(() => {
        setSucess(true)
        const timer = setTimeout(() => {
          history('/catalogo')
        }, 1000)
      })
      .catch((err) => {
        console.log(err)
      })
  }

  const handleClose = (
    event?: React.SyntheticEvent | Event,
    reason?: string
  ) => {
    setSucess(false)
  }
  return (
    <>
      <div className="w-96	  bg-[#fff4f0] ">
        <div className="mt-0 flex justify-center  ">
          <img src={logo} className="h-48 w-48" />
        </div>
        <div className="p-1	  text-center shadow-gray-500">
          <Typography variant="h4">Bem vindo</Typography>
          <p className="mb-10 text-zinc-400">
            Aliquan consectetur et tincidunt praesent erim massa pelioentest
            velit odieo neque
          </p>
        </div>
        <div className="grid justify-items-center ">
          <form onSubmit={formik.handleSubmit}>
            <div className="grid w-80 gap-5">
              <div className="mt1 grid gap-3  rounded-md">
                <TextField
                  name="username"
                  className="  border- bg-[#a77565]outline-none w-full flex-1 rounded-none  rounded-r-md border focus:border-[#a77565] focus:ring-[#a77565] sm:text-sm"
                  placeholder={'Nome do usuário'}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.username}
                  label="Nome do usuário"
                  variant="outlined"
                  size="small"
                  error={
                    formik.touched.username && Boolean(formik.errors.username)
                  }
                  helperText={formik.touched.username && formik.errors.username}
                />

                <TextField
                  name="email"
                  className="  border- bg-[#a77565]outline-none w-full flex-1 rounded-none  rounded-r-md border focus:border-[#a77565] focus:ring-[#a77565] sm:text-sm"
                  placeholder={'Nome do usuário'}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.email}
                  label="Email"
                  variant="outlined"
                  type="email"
                  size="small"
                  error={formik.touched.email && Boolean(formik.errors.email)}
                  helperText={formik.touched.email && formik.errors.email}
                />
                <TextField
                  name="password"
                  className="  border- bg-[#a77565]outline-none w-full flex-1 rounded-none  rounded-r-md border focus:border-[#a77565] focus:ring-[#a77565] sm:text-sm"
                  placeholder={'Endereço'}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.password}
                  label="senha"
                  variant="outlined"
                  size="small"
                  error={
                    formik.touched.password && Boolean(formik.errors.password)
                  }
                  helperText={formik.touched.password && formik.errors.password}
                  type="password"
                />
              </div>

              <Button textButton="Registrar" type="submit" name="submit" />
            </div>
          </form>
        </div>
      </div>
      <Snackbar
        open={sucesss}
        autoHideDuration={6000}
        onClose={handleClose}
        message="usuário cadastrado"
      >
        <Alert severity="success">Usuario cadastrado com sucesso!!</Alert>
      </Snackbar>
    </>
  )
}
