import { ButtonHTMLAttributes } from 'react'

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  textButton: string
}

export const Button = ({ textButton, ...props }: ButtonProps) => {
  return (
    <button
      {...props}
      className={
        props.className +
        ' h-10 rounded-md  border-none border-black bg-[#a77565] text-slate-50 hover:bg-[#311a07] focus:outline-none focus:ring-1'
      }
    >
      {textButton}
    </button>
  )
}
