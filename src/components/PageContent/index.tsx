import { Box, BoxProps } from '@material-ui/core'

interface PageContentProps extends BoxProps {
  children: React.ReactNode
}
export const PageContent = ({ children, ...props }: PageContentProps) => {
  return <Box className={`${props.className} m-auto h-screen`}>{children}</Box>
}
