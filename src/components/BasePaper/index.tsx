import { Box, BoxProps } from '@material-ui/core'
import React from 'react'

interface BasePaperProps extends BoxProps {
  children: React.ReactNode
}

export const BasePaper = ({ children, ...props }: BasePaperProps) => {
  return (
    <Box className={`${props.className} gap-5	shadow-gray-700`}>{children}</Box>
  )
}
