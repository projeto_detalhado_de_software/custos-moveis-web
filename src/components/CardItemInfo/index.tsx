import { useNavigate } from 'react-router-dom'
import item from 'assets/itemTest.svg'
import IconButton from '@material-ui/core/IconButton'
import CreateIcon from '@material-ui/icons/Create'
import { Box } from '@material-ui/core'
interface CardItemMinInfo {
  productName: string
  productprice: string
}
export const CardItemInfo = ({
  productName,
  productprice
}: CardItemMinInfo) => {
  const history = useNavigate()

  return (
    <Box
      className="  relative w-64 rounded bg-white"
      onClick={() => {
        history('/view')
      }}
    >
      <div className="bg-white ">
        <div className="border-5 absolute z-10  flex w-[256px]  justify-end 	 ">
          <IconButton
            onClick={(e) => {
              history(`/edit-product/${1}`)
              e.stopPropagation()
            }}
          >
            <CreateIcon fontSize="small" />
          </IconButton>
        </div>
        <img className="h-[100%] w-[256px]" src={item} />
      </div>
      <div className="h-[4.875rem] bg-white">
        {/*Name*/}
        <div className="py-2 ">
          <p className="prose-base px-3 font-['Saira'] text-base	   leading-6">
            {productName}
          </p>
        </div>
        <div className="border-[DCE2E5] mx-auto h-[1px] w-[14.25rem] border" />
        <div className="">
          <p className="px-3 font-bold">R$ {productprice}</p>
        </div>
      </div>
    </Box>
  )
}
