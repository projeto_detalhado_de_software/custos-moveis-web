import { Box, Typography } from '@material-ui/core'

interface TitilePageProps {
  text: string
}
export const TitlePage = ({ text }: TitilePageProps) => {
  return (
    <Box className="mb-5 ">
      <Typography variant="h3" className="text-[#a77565]">
        {text}
      </Typography>
    </Box>
  )
}
