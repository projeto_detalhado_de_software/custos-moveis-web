import { TextField, TextFieldProps } from '@material-ui/core'

export const TextFieldStyled = ({ ...props }: TextFieldProps) => {
  return (
    <TextField
      className="border- bg-[#a77565]outline-none w-full flex-1 rounded-none  rounded-r-md border focus:border-[#a77565] focus:ring-[#a77565] sm:text-sm"
      variant="outlined"
      size="small"
      {...props}
    />
  )
}
