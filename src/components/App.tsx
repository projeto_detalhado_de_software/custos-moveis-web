import { Home } from 'Page/Home'

function App() {
  return (
    <div className="h-screen w-screen ">
      <Home />
    </div>
  )
}

export default App
