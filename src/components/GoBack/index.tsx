import { Link } from '@material-ui/core'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import { useNavigation, useNavigate } from 'react-router-dom'

interface GoBackProps {
  route?: string
}
export const GoBack = ({ route }: GoBackProps) => {
  const navigate = useNavigate()
  return (
    <Link
      onClick={() => {
        route ? navigate(`/${route}`) : navigate(-1)
      }}
    >
      <ArrowBackIcon />
      Voltar
    </Link>
  )
}
