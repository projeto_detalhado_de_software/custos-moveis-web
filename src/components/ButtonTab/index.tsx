import { Button } from '@material-ui/core'
import { ReactNode } from 'react'

interface ButtonTab {
  children: ReactNode
  onClick: () => void
}
export const ButtonTab = ({ children, onClick }: ButtonTab) => (
  <Button
    className="border-1 border border-[#a77565]"
    onClick={() => onClick()}
  >
    {children}
  </Button>
)
