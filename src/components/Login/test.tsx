import { render, screen, waitFor } from '@testing-library/react'

import { Login } from '.'

describe('<Login/>', () => {
  it('should render the login', () => {
    render(<Login />)

    expect(screen.getByText('Olá novamente')).toBeInTheDocument()
  })
})
