import { useState } from 'react'
import * as Yup from 'yup'
import { useNavigate } from 'react-router-dom'
import { useFormik } from 'formik'
import axios from 'axios'
import { Snackbar, TextField, Typography } from '@material-ui/core'
import { Alert } from '@material-ui/lab'

import { baseURl } from 'services'
import { Button } from 'components/Buttom'
import logo from 'assets/custos-moveis.png'

export const Login = () => {
  const [sucesss, setSucess] = useState(false)
  const [failed, setFailed] = useState(false)

  const history = useNavigate()

  const formik = useFormik({
    initialValues: {
      username: '',
      password: ''
    },
    validationSchema: Yup.object({
      username: Yup.string().required('Obrigatório'),
      password: Yup.string().required('Obrigatório')
    }),
    onSubmit: (values) => {
      loginRequest(values)
    }
  })

  const handleClose = () => {
    setSucess(false)
    setFailed(false)
  }

  const loginRequest = async (values: {
    username: string
    password: string
  }) => {
    await axios
      .post(baseURl + 'auth', { ...values })
      .then((response) => {
        sessionStorage.setItem('token', response.data.token)
        setSucess(true)
        const timer = setTimeout(() => {
          history('/catalogo')
        }, 1000)
      })
      .catch((err) => {
        setFailed(true)
      })
  }
  return (
    <>
      <div className="w-96	  bg-[#fff4f0]">
        <div className="mt-0 flex justify-center  ">
          <img src={logo} className="h-48 w-48" />
        </div>
        <div className="p-1	  text-center shadow-gray-500">
          <Typography variant="h4">Olá novamente</Typography>
          <p className="mb-10 text-zinc-400">
            Olá, seja bem vindo ao custo móveis, seu gerenciador de produtos 👍
          </p>
        </div>
        <div className="grid justify-items-center ">
          <form onSubmit={formik.handleSubmit}>
            <div className="grid w-80 gap-5">
              <div className="mt1 grid gap-3  rounded-md">
                <TextField
                  name="username"
                  className="  border- bg-[#a77565]outline-none w-full flex-1 rounded-none  rounded-r-md border focus:border-[#a77565] focus:ring-[#a77565] sm:text-sm"
                  placeholder={'Nome de usuário'}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.username}
                  label="Nome de usuário"
                  variant="outlined"
                  size="small"
                  error={
                    formik.touched.username && Boolean(formik.errors.username)
                  }
                  helperText={formik.touched.username && formik.errors.username}
                />

                <TextField
                  name="password"
                  type="password"
                  className="  border- bg-[#a77565]outline-none w-full flex-1 rounded-none  rounded-r-md border focus:border-[#a77565] focus:ring-[#a77565] sm:text-sm"
                  placeholder={'senha'}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.password}
                  label="senha"
                  variant="outlined"
                  size="small"
                  error={formik.errors.password ? true : false}
                />
              </div>

              <Button textButton="Login" type="submit" name="submit" />
            </div>
          </form>
        </div>
        <Snackbar
          open={sucesss}
          autoHideDuration={6000}
          onClose={handleClose}
          message="usuário cadastrado"
        >
          <Alert severity="success">Login realizado com sucesso</Alert>
        </Snackbar>

        <Snackbar
          open={failed}
          autoHideDuration={6000}
          onClose={handleClose}
          message="usuário cadastrado"
        >
          <Alert severity="error">Falha no login</Alert>
        </Snackbar>
      </div>
    </>
  )
}
