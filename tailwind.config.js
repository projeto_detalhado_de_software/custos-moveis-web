/** @type {import('tailwindcss').Config} */

module.exports = {
  content: ['./src/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {
      backgroundImage: {
        woodwork: "url('/assets/marcenaria.jpg')"
      }
    }
  },
  plugins: []
}
